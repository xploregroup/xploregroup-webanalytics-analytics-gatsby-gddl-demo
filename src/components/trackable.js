import React , { Component } from "react"
import PropTypes from "prop-types"
import analyticstracker from "analyticstracker"

class Trackable extends Component {
    componentDidMount = () => {
        if (this.props.analyticsData.event === "page-impression") {
            // console.log("Page component");
            console.log(analyticstracker())
            analyticstracker().trackImpression("page-impression", {"newPage" : true});
            setTimeout(function trackAll(){
                analyticstracker().trackImpression("promotion-impression");
                analyticstracker().trackImpression("contentblock-impression");
                analyticstracker().trackImpression("button-impression");
                setTimeout(trackAll, 1000);
            }, 1000);
        } else {
            // console.log("Child component");
        }
    }

    handleClick = (event) => {
        let clickEvent = this.props.analyticsData.event.replace("impression","click")
        analyticstracker().trackInteraction(event, {changeEvent: clickEvent})
    }

    render () {
        return (
            <div role="none"
            data-tracking-event={this.props.analyticsData.event}
            data-tracking-info={JSON.stringify(this.props.analyticsData.info)}

            onClick={this.props.isClickable? this.handleClick : () => {}}
            >
                {this.props.children}
            </div>
        )
    }

}

Trackable.propTypes = {
    analyticsData: PropTypes.object,
    children: PropTypes.node,
    isClickable: PropTypes.bool
}

Trackable.defaultProps = {
    analyticsData: {},
    isClickable: false
}
 
export default Trackable