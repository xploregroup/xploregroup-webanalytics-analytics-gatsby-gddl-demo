import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import buttonStyles from "./button.module.css"

const Button = ({ buttonText, to }) => (
    <Link className={buttonStyles.myButton} to={to} >{buttonText}</Link>
)

Button.propTypes = {
    buttonText: PropTypes.string,
    to: PropTypes.string
}
  
Button.defaultProps = {
    buttonText: ``,
}
  
export default Button