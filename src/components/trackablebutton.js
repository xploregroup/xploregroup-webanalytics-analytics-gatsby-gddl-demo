import { Link } from "gatsby"
import React from "react"
import buttonStyles from "./trackablebutton.module.css"
import analyticstracker from "analyticstracker"

const TrackableButton = ({ buttonText, to, analyticsData }) => (
    <div role="none"
            data-tracking-event={analyticsData.event}
            data-tracking-info={JSON.stringify(analyticsData.info)}
            >
        <Link data-tracking-event={analyticsData.event.replace("impression","click")}
                data-tracking-info={JSON.stringify(analyticsData.info)}
                className={buttonStyles.myTButton} to={to} onClick={(typeof window !== "undefined") ? analyticstracker().trackInteraction : null}>{buttonText}</Link>
    </div>
)
  
export default TrackableButton