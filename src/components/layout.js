/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Trackable from "./trackable"
import Header from "./header"
import "./layout.css"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
      <Trackable isClickable={true} analyticsData={{event:"promotion-impression", info:{name: data.site.siteMetadata.title ,position:"top"}}}>
        <Header siteTitle={data.site.siteMetadata.title}/>
      </Trackable>
      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0 1.0875rem 1.45rem`,
        }}
      >
        <main>{children}</main>
        <Trackable isClickable={true} analyticsData={{event:"promotion-impression", info:{name: "some Footer Banner" ,position:"bottom"}}}>
          <Header siteTitle="some Footer Banner"/>
        </Trackable>
        <footer>
          © {new Date().getFullYear()}, Built with
          {` `}
          <a href="https://www.gatsbyjs.org">Gatsby</a>
        </footer>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
