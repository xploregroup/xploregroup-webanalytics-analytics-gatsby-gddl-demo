import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import Trackable from "../components/trackable"
import Button from "../components/button"

const IndexPage = () => (
  <>
    <Trackable analyticsData={{event:"page-impression", info:{name: "Home Page" ,author:"stefan"}}}></Trackable>
    <Layout>
      <SEO title="Home" />
      <h1>Hi people</h1>
      <Trackable analyticsData={{event:"contentblock-impression", info:{name: "Main content" ,position:"middle"}}}>
        <p>Welcome to your new Gatsby site.</p>
        <p>Now go build something great.</p>
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
          <Image />
        </div>
      </Trackable>
      <div>
        <Trackable isClickable={true} analyticsData={{event:"button-impression", info:{name: "CTA" ,position:"middle"}}}>
          <Button buttonText="CTA" to="/page-2/"/>
        </Trackable>
      </div>
      <Link to="/page-2/">Go to page 2</Link>
    </Layout>
  </>
)

export default IndexPage
