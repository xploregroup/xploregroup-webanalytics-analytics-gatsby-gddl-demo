import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Trackable from "../components/trackable"
import TrackableButton from "../components/trackablebutton"

const SecondPage = ( ) => (
  <>
    <Trackable isClickable={false} analyticsData={{event:"page-impression", info:{name: "Second Page" ,author:"stefan"}}} />
      <Layout>
        <SEO title="Second Page" />
        <Trackable isClickable={false} analyticsData={{event:"contentblock-impression", info:{name: "Second page content" ,position:"middle"}}}>
          <h1>Hi from the second page</h1>
          <p>Welcome to page 2</p>
        </Trackable>
        <div>
          <TrackableButton buttonText="TR-CTA" to="/" analyticsData={{event:"button-impression", info:{name: "Native trackable CTA" ,position:"middle"}}} />
        </div>
        <Link to="/">Go back to the homepage</Link> 
      </Layout>
  </>
)

export default SecondPage